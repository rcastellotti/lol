import requests
import os
import json
import datetime
import pytz
import sys
from rich.console import Console
from dotenv import load_dotenv

load_dotenv()
console = Console()

def telegram_send_message():
    telegram_token = os.environ["TELEGRAM_TOKEN"]
    telegram_url = f"https://api.telegram.org/bot{telegram_token}/sendMessage?chat_id=@badc0ff3_status"
    date = datetime.datetime.now(pytz.timezone("Europe/Rome")).strftime('%d/%m/%Y %H:%M:%S %Z')

    text = f"""
    cronjob running:
date: `{date}`
updated [s\.rcastellotti\.dev/lol\.json](https://s\.rcastellotti\.dev/lol\.json)
check [lol\.rcastellotti\.dev](https://lol\.rcastellotti\.dev)
    """

    params = {
        "text": text,
        "parse_mode": "MarkdownV2"
    }
    requests.get(telegram_url, params=params)


# fragolaqt paoloidolo darkchri999
ids = ["_Hg2bTxDifqjTDAsL5IsbuaUmqXWYtyJlG3VAJM0_gr88pw",
       "uZUWFSOlpgJwZGWmOgWglxkjMnSDFHN-s6e5fsdldvOdxB4",
       "C2DYJuNv77nQfHDAOC3k2_ofWIQx-L5dVBNcnzuA2XBposk"]

def main(ids):
    token = os.environ["TOKEN"]
    url_lp = "https://euw1.api.riotgames.com/lol/league/v4/entries/by-summoner/"
    headers = {'X-Riot-Token': token}

    for id in ids:
        r = requests.get(url_lp+id, headers=headers)
        lp=0
        i=0 if (r.json()[0]["queueType"]=="RANKED_SOLO_5x5") else 1
        if r.json()[i]["tier"]!="DIAMOND":
            lp = r.json()[i]["leaguePoints"]        
        
        with open('data/lol.json', 'r') as f:
            data = json.load(f)

        date = datetime.date.strftime(datetime.datetime.now(), "%Y-%m-%d")
       
        if not id in data:
            data[id]=[]
        if (len(data[id])>0 and data[id][-1]["x"] == date):
                data[id][-1]["y"] = lp
        else:
            new = dict({
                "x": date,
                "y": lp
            })

            data[id].append(new)

        with open('data/lol.json', 'w') as f:
            json.dump(data, f, indent=4)
    telegram_send_message()
    
def id(id):
    url_id=f"https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/{id}"
    token = os.environ["TOKEN"]
    headers = {'X-Riot-Token': token}
    r=requests.get(url_id,headers=headers)
    return(r.json())


if __name__ == "__main__" and len(sys.argv)<=1:
    main(ids)
else:
    console.log(id(sys.argv[2]))
    
