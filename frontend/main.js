const fragolaqt = "_Hg2bTxDifqjTDAsL5IsbuaUmqXWYtyJlG3VAJM0_gr88pw";
const darkchri999 = "C2DYJuNv77nQfHDAOC3k2_ofWIQx-L5dVBNcnzuA2XBposk";
const paoloidolo = "uZUWFSOlpgJwZGWmOgWglxkjMnSDFHN-s6e5fsdldvOdxB4";

function handleErrors(res) {
    if (!res.ok) throw new Error(res.error);
    return res;
}

fetch("https://s.rcastellotti.dev/lol.json", { cache: "no-store" })
    .then((response) => {
        if (response.status != "404") return response.json();
    })

    .then(function (data) {
        let ctx = document.getElementById("myChart").getContext("2d");
        var chart = new Chart(ctx, {
            type: "scatter",
            data: {
                datasets: [
                    {
                        label: "fragolaqt",
                        borderColor: "rgb(255,0,0)",
                        tension: 0,
                        showLine: true,
                        fill: false,
                        data: data[fragolaqt],
                    },
                    {
                        label: "darkchri999",
                        data: data[darkchri999],
                        borderColor: "rgb(118,180,238)",
                        tension: 0,
                        showLine: true,
                        fill: false,
                    },
                    {
                        label: "paoloidolo",
                        data: data[paoloidolo],
                        borderColor: "rgb(0,97,63)",
                        tension: 0,
                        showLine: true,
                        fill: false,
                    },
                ],
            },
            options: {
                elements: {
                    line: {
                        tension: 0,
                    },
                },
                title: {
                    display: true,
                    text: "Top Twitch League of Legends Italian Streamers",
                },
                scales: {
                    xAxes: [
                        {
                            type: "time",
                            time: {
                                tooltipFormat: "DD/MM/YYYY",
                                unit: "day",
                                displayFormats: {
                                    day: "DD/MM/YYYY",
                                },
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "date",
                            },
                        },
                    ],
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "LPs",
                            },
                        },
                    ],
                },
            },
        });
        document.getElementById("error").remove();
    })
    .catch((error) => {
        console.error(error);
        document.getElementById("myChart").remove();
        error_msg = "Hey it's not you, it's me, i f****d up something";
        document.getElementById("error").textContent = error_msg;
    });
