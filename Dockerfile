FROM python:alpine
ADD lol.py requirements.txt /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN ls
CMD ["python3","lol.py"]